new WOW().init();



/////////////////////////// MENU ////////////////////////////

// Works everywhere
$(document).ready(function () {

  // Hide/show animation hamburger function
  $('.navbar-toggler').on('click', function () {

    // Take this line to first hamburger animations
    $('.animated-icon1').toggleClass('open');

    // Take this line to second hamburger animation
    $('.animated-icon3').toggleClass('open');

    // Take this line to third hamburger animation
    $('.animated-icon4').toggleClass('open');
  });

});
/////////////////////////////////////////////////////////////
// Material Select Initialization
$(document).ready(function() {
  if(typeof($('.mdb-select').material_select) == 'function')
   $('.mdb-select').material_select();
 });

