import StampDay from './StampDay';

export default class StampByDay {

    constructor($obj) {
      this._monthLenght = $obj.monthLenght;
      this._monthOldClientsStampsQuantity = $obj.monthOldClientsStampsQuantity;
      this._monthNewClientsStampsQuantity = $obj.monthNewClientsStampsQuantity;
      this._monthAllClientsStampsQuantity = $obj.monthAllClientsStampsQuantity;
      this._monthOldClientsQuantity = $obj.monthOldClientsQuantity;
      this._monthNewClientsQuantity = $obj.monthNewClientsQuantity;
      this._monthAllClientsQuantity = $obj.monthAllClientsQuantity;
      this._allClientsQuantity = $obj.allClientsQuantity;
      this._stampByDayList = this.mountStampByDayList($obj.stampByDayList);
    }

    mountStampByDayList(list) {
        return list.map( (element) => {
            return new StampDay(element);
        });
    }

    getMonthLenght() {
        return this._monthLenght;
    }

    getMonthOldClientsStampsQuantity() {
        return this._monthOldClientsStampsQuantity;
    }

    getMonthNewClientsStampsQuantity() {
        return this._monthNewClientsStampsQuantity;
    }

    getMonthAllClientsStampsQuantity() {
        return this._monthAllClientsStampsQuantity;
    }

    getMonthOldClientsQuantity() {
        return this._monthOldClientsQuantity;
    }

    getMonthNewClientsQuantity() {
        return this._monthNewClientsQuantity;
    }

    getMonthAllClientsQuantity() {
        return this._monthAllClientsQuantity;
    }

    getAllClientsQuantity() {
        return this._allClientsQuantity;
    }

    getStampByDayList() {
        return this._stampByDayList.slice(0);
    }

    getFormatedList() {
        return this.getStampByDayList().map( (stampDay) => {
            return {
                Dia: stampDay.getDayNumber(), 
                Novos: stampDay.getCurrentStampsQuantity(), 
                Antigos: stampDay.getOldStampsQuantity(),
            }
        });
    }
  }