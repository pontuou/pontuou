export default class StampByDay {

  constructor($obj){
      this._dayNumber = $obj.dayNumber;
      this._currentStampsQuantity = $obj.currentStampsQuantity;
      this._oldStampsQuantity = $obj.oldStampsQuantity;
  }

  getDayNumber() {
      return this._dayNumber;
  }

  getCurrentStampsQuantity() {
      return this._currentStampsQuantity;
  }

  getOldStampsQuantity() {
      return this._oldStampsQuantity;
  }
}