export function setFocus(htmlElement) {
  setTimeout(() => {
    if (htmlElement) htmlElement.focus();
  }, 250)
}