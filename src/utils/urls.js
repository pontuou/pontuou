// DEVELOMENT 
//const base = 'https://pontuou-srv-desenvolvimento.herokuapp.com/v1.5/';

// PRODUCTION
const base = 'https://pontuou-srv-production.herokuapp.com/v1.5/';

export const urlLogin = base + 'login';

export const urlExistsClient = base + 'finalClients/existClientByUIAndEnterpriseId';

export const urlCreateUser = base + 'finalClients/createWithEmptyCard';

export const urlStamp = base + 'stamps/addStampsByFinalClientIdAndEnterpriseId/';

export const urlDeliveryPrize = base + 'cards/getReward/';

export const urlCreateCard = base + 'cards/createEmptyCard';

export const urlStampByDay = base + 'reports/getStampQuantityPerDayByMonth';

export const clientsByInterpriseId = base + 'finalClients/findByEnterpriseId';

export const clientsByStampQuantity = base + 'finalClients/findByStampQuantity';

export const urlClientsSince = base + 'finalClients/findByLastVisitDateBefore';