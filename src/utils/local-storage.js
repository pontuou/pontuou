export const tokenTag = "pnt-token";
export const enterpriseIdTag = "pnt-enterprise-id";
export const enterpriseUrlLogoTag = "pnt-enterprise-url-token";
export const enterpriseOfferQuantityTag = "pnt-enterprise-offer-quantity";
export const enterpriseNameTag = "pnt-enterprise-name";
export const enterpriseOfferTypeTag = "pnt-enterprise-offer-type";
export const enterpriseTag = "pnt-enterprise";

export function getData() {
  return {
    token: localStorage.getItem(tokenTag),
    enterpriseId: localStorage.getItem(enterpriseIdTag),
    enterpriseUrlLogo: localStorage.getItem(enterpriseUrlLogoTag),
    enterpriseOfferQuantity: localStorage.getItem(enterpriseOfferQuantityTag),
    enterpriseOfferType: localStorage.getItem(enterpriseOfferTypeTag),
    enterpriseName: localStorage.getItem(enterpriseNameTag)
  }
}

export function setToken(token) {
  localStorage.setItem(tokenTag, token)
}

export function setEnterpriseId(id) {
  localStorage.setItem(enterpriseIdTag, id)
}

export function setEnterpriseName(name) {
  localStorage.setItem(enterpriseNameTag, name)
}

export function setEnterpriseUrlLogo(urlLogo) {
  localStorage.setItem(enterpriseUrlLogoTag, urlLogo)
}

export function setEnterpriseOfferQuantity(quantity) {
  localStorage.setItem(enterpriseOfferQuantityTag, quantity)
}

export function setEnterpriseOfferType(type) {
  localStorage.setItem(enterpriseOfferTypeTag, type)
}

export function clearKey(key){
  localStorage.removeItem(key)
}

export function checkToken() {
  return getData().token === null;
}

// ---------------
// Using SESSION
// ---------------

export function getOffers(){
  return getSession().enterprise.offers;
}

export function getEnterpriseId(){
  try{
    return getSession().enterprise.id;
  }catch (e){
    return false;
  }
}

export function getEnterpriseName(){
  try{
    return getSession().enterprise.name;
  }catch (e){
    return false;
  }
}

export function setSession(object) {
  sessionStorage.setItem(enterpriseTag, JSON.stringify(object));
}

export function getSession() {
  return JSON.parse(sessionStorage.getItem(enterpriseTag));
}

export function clearSession(){
  sessionStorage.clear();
}

export function clearAllKeys() {
  clearKey(tokenTag);
  clearKey(enterpriseIdTag);
  clearKey(enterpriseUrlLogoTag);
  clearKey(enterpriseOfferQuantityTag);
  clearKey(enterpriseNameTag);
  clearKey(enterpriseOfferTypeTag);
  clearSession();
}
