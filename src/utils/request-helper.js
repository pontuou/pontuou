import axios from 'axios';
import {getData, clearAllKeys} from './local-storage';

/*
  @method - Metodo da raquisicao
  @url    - URL da requisicao
  @body   - Corpo da requisicao, no formato JSON
  @auth   - Enviar HEADER de autenticacao?
*/

export default function(method, url, body, loadElement = 'load', auth = false) {
  const headers = {
    "Content-Type": "application/json",
  }

  if(auth) headers['Authorization'] = getData().token;

  const options = {
    headers
  }
  
  let element = document.getElementsByClassName(loadElement)[0]
  if (!element) element = document.getElementById('AppMainRoot')
  element.classList.add('loader');

  return axios[method](url, body, options).then(res => {
    //console.log(res)
    element.classList.remove('loader');
    return res.data;
  })
  .catch(err => {
    console.log(`Error!`, ` Method: ${method} | URL: ${url}`);
    element.classList.remove('loader');
    if(err.response.status === 401){
      if(getData().enterpriseId){
        alert('Sua sessao expirou. Refaca o login!');
        clearAllKeys();
        document.location.reload();
      }
      else
      {
        alert('Usario/senha invalidos');
      }

    } 
    throw err.response;
  })
}
