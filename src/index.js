import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import thunkMiddleware from 'redux-thunk';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import registerServiceWorker from './registerServiceWorker';

// REDUCERS ------------------------
import LoginReducer from './reducers/login';
import PurchaseReducer from './reducers/purchase';


// PAGINAS ---------------------

import Login from './pages/login';
import Stamp from './pages/stamp';
import FirstBuy from './pages/first-buy';
import FirstTime from './pages/first-time';
import ConfirmStamp from './pages/confirm-stamp';
import Dashboard from './pages/dashboard';
import Clients from './pages/clients';
import ChooseOffer from './pages/choose-offer';
import ClientsSince from './pages/reports/ClientsSince';

const reducers = combineReducers({LoginReducer, PurchaseReducer});
const store = createStore(reducers, applyMiddleware(thunkMiddleware))

ReactDOM.render(
  <Provider store={store}>  
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login}/>
        <Route exact path="/stamp" component={Stamp}/>
        <Route exact path="/first-buy" component={FirstBuy}/>
        <Route exact path="/first-time" component={FirstTime}/>
        <Route exact path="/confirm-stamp" component={ConfirmStamp}/>
        <Route exact path="/dashboard" component={Dashboard}/>
        <Route exact path="/clients" component={Clients}/>
        <Route exact path="/choose-offer" component={ChooseOffer}/>
        <Route exact path="/clients-since" component={ClientsSince}/>
        <Route path="/*" component={Login}/>
      </Switch>
    </BrowserRouter>
  </Provider>
    , document.getElementById('AppMainRoot'));
registerServiceWorker();
