const defaultState = {
  logged_in: null
}

export default function(state = defaultState, action){
  const newState = Object.assign({}, defaultState);

  if(action.type === 'LOGIN') {
    newState.logged_in = action.content.startsWith('Bearer');
  }

  return newState;
}