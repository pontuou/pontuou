const defaultState = {
  client_exists: undefined,
  client_dont_exists: undefined,
  register_success: undefined,
  stamp_success: undefined
}

export default function(state = defaultState, action){
  const newState = Object.assign({}, defaultState);

  if(action.type === 'CLIENT-EXISTS') {
    newState.client_exists = action.content;
  }

  if(action.type === 'CLIENT-DONT-EXISTS') {
    newState.client_dont_exists = action.content;
  }

  if(action.type === 'REGISTER-SUCCESS') {
    newState.register_success = action.content;
  }

  if(action.type === 'STAMP-SUCCESS') {
    newState.stamp_success = action.content;
  }

  return newState;
}