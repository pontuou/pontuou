import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import WOW from 'wowjs';
import StackedBarChart from './components/StackedBarChart';
import RequestHelper from '../utils/request-helper';
import { urlStampByDay } from '../utils/urls';
import {  checkToken, getEnterpriseId } from '../utils/local-storage';
import MainPanel from './components/MainPanel';
import { months } from '../utils/date';
import StampByDay from '../models/StampByDay';

class Stamp extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});

    const year = (new Date()).getFullYear();

    this.state = {
      month: (new Date().getMonth() + 1),
      year: year,
      years: Array.from(new Array(5),(val, index) => (index -1) + (year)),
      stampByDay: null,
      list: []
    }
  }

  componentDidMount(){
    this.WOW.init();
    this.getData(new Date());
  }

  getData = (date) => {
    const enterpriseId = getEnterpriseId();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    this.setState({year: year});

    RequestHelper('post', urlStampByDay, { enterpriseId, month, year }, 'load', true)
    .then(res => {
      //console.log(res)
      const stampByDay =  new StampByDay(res);
      this.setState(() => ({
        data: res, 
        stampByDay: stampByDay, 
        list: stampByDay.getFormatedList()
      }))
    })
    .catch(err => {
      //console.log(err);
    })
  }

  setMonthYear = (month, year) => {
    const date = new Date();
    date.setMonth(month - 1);
    date.setYear(year);
    return date;
  }

  handleSelectMonth = event => {
    this.setState({month: event.target.value});
    this.getData(this.setMonthYear(event.target.value, this.state.year));
  }

  handleSelectYear = event => {
    this.setState({year: event.target.value});
    this.getData(this.setMonthYear(this.state.month, event.target.value));
  }

  render() {
    const {month, stampByDay, list, year, years} = this.state;    
    if(checkToken()) return <Redirect push to="/"/>

    return (
      <MainPanel>
        <br/>
        <div className="container">

          <div className="row">

            <div className="col-md-8 p-0">
              <div className="card stamp m-2 p-0">
                <h4 className="text-center card-header">
                  <select
                    className="mdb-select large-select"
                    defaultValue={month}
                    onChange={this.handleSelectMonth}
                  >
                    {months.map((month, index) => {
                      return <option value={index + 1} key={index}>{month}</option>
                    })}
                  </select>
                  <select
                    className="mdb-select large-select mt-1 ml-2 mr-2"
                    defaultValue={year}
                    onChange={this.handleSelectYear}
                  >
                    {years.map((year, index) => {
                      return <option value={year} key={index}>{year}</option>
                    })}
                  </select>
                </h4>
                <div className="card-body text-center load" >
                  {stampByDay && (
                    <div style={{height: "400px"}} >
                      <StackedBarChart list={list} />
                    </div>
                  )}
                </div>
              </div>
            </div>
            
            <div className="col-md-4">
              <div className="row">

                <div className="col-md-10 card stamp m-2 p-0">
                  <h4 className="text-center card-header">Relatório geral</h4>
                  <div className="card-body text-center load" >
                      <p className="label-info p-3">
                        Total de clientes cadastrados: 
                      </p>
                      <strong className="number" >{stampByDay && stampByDay.getAllClientsQuantity()}</strong>
                  </div>
                </div>

                <div className="col-md-10 card stamp m-2 p-0">
                  <h4 className="text-center card-header">Relatório mensal</h4>
                  <div className="card-body text-center load" >
                    <p className="label-info p-3">
                        Carimbos no mês: 
                      </p>
                      <strong className="number" >{stampByDay && stampByDay.getMonthAllClientsStampsQuantity()}</strong>
                      <hr/>
                      <p className="label-info p-3">
                        Carimbos de clientes antigos no mês: 
                      </p>
                      <strong className="number" >{stampByDay && stampByDay.getMonthOldClientsStampsQuantity()}</strong>
                      <hr/>
                      <p className="label-info p-3">
                        Carimbos de clientes novos no mês: 
                      </p>
                      <strong className="number" >{stampByDay && stampByDay.getMonthNewClientsStampsQuantity()}</strong>
                      <hr/>
                      <p className="label-info p-3">
                        Clientes antigos no mês: 
                      </p>
                      <strong className="number" >{stampByDay && stampByDay.getMonthOldClientsQuantity()}</strong>
                      <hr/>
                      <p className="label-info p-3">
                        Clientes novos no mês: 
                      </p>
                      <strong className="number" >{stampByDay && stampByDay.getMonthNewClientsQuantity()}</strong>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </MainPanel>
    )
  }
}

export default Stamp;
