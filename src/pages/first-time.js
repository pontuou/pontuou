import React, {Component} from 'react';
import FlatButton from './components/FlatButton';
import {Redirect, Link} from 'react-router-dom';
import WOW from 'wowjs';
import RequestHelper from '../utils/request-helper';
import { urlCreateCard } from '../utils/urls';
import { getData, checkToken } from '../utils/local-storage';
import MainPanel from './components/MainPanel';

class FirstTime extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      create_success: undefined,
      error: undefined
    }
  }

  componentDidMount(){
    this.WOW.init();
  }

  createCard = () => {
    const finalClientId = this.props.location.clientData.finalClientId;
    const enterpriseId = getData().enterpriseId;

    RequestHelper('post', urlCreateCard, {finalClientId, enterpriseId}, 'load', true)
    .then(res => {
      this.setState( () => {
        return { create_success: res}
      })
    })
    .catch(err => {
      ////console.log(err);
    })
  }



  render() {
    if(checkToken()) return <Redirect push to="/"/>

    const data = this.props.location.clientData;
    ////console.log(data)
    ////console.log(this.state)

    if (this.state.create_success) {
      return <Redirect push to={{pathname: "/confirm-stamp", currentData: this.state.create_success}} />
    }

    if (!data) {
      return <Redirect push to="/stamp" />
    }

    return (
      <MainPanel>
        <div className="card mx-xl-5 stamp">
          <div className="card-body text-center load">
            <h2>Primeira compra de <b>{data.finalClientEmail}</b>. Confirmar criação do cartão?</h2>
            <br/>
              <FlatButton icon="fas fa-check ml-2" text="Sim" type="button" click={this.createCard}/>
              <Link to="/stamp">
                <FlatButton icon="fas fa-arrow-left  ml-2" text="Voltar" type="button"/>
              </Link>
              </div>
          </div>
      </MainPanel>
    )
  }

}

export default FirstTime;
