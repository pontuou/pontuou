import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import WOW from 'wowjs';
import { setFocus } from '../utils/helpers';
import { checkToken, getOffers } from '../utils/local-storage';
import RequestHelper from '../utils/request-helper';
import { urlCreateUser } from '../utils/urls';
import FlatButton from './components/FlatButton';
import MainPanel from './components/MainPanel';
import Message from './components/Message';

class FirstBuy extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      register_success: undefined,
      error: undefined,
      chooseOffer: undefined,
      clientData: this.props.location.clientData,
      offerData: undefined,
    }
    this._input = null;
  }

  componentDidMount(){
    this.WOW.init();
    setFocus(this._input)
  }

  register = event => {
    event.preventDefault();

    const finalClientEmail = event.target.elements.name.value;
    const finalClientUI = this.state.clientData;

    if (getOffers().length > 1) {
      this.setState(() => ({chooseOffer: true}))
    }

    const offerId = getOffers()[0].id;

    RequestHelper('post', urlCreateUser, {finalClientUI, finalClientEmail, offerId}, 'load', true)
    .then(res => {
      //console.log(res);
      this.setState(() => {
        return {
          clientData: res,
          offerData: getOffers()
        }
      })
    })
    .catch(err => {
      ////console.log(err)
      this.setState(() => {
        return {
          error: err.data.errors
        }
      })
    })
  }

  render() {
    if(checkToken()) return <Redirect push to="/"/>
    if(!this.state.clientData) {
      return <Redirect to="/stamp" />
    }

    if (this.state.clientData.cards && getOffers().length > 1)
      return <Redirect to={{
        pathname: "/choose-offer", 
        clientData: this.state.clientData,
        offerData: this.state.offerData
        }} />
    else if (this.state.clientData.cards)
      return <Redirect to={{
        pathname: "/confirm-stamp", 
        clientData: this.state.clientData,
        offerData: this.state.offerData[0]
      }} />

    return (

      <MainPanel>
        <form onSubmit={this.register} >
          <div className="firsbtuy card mx-xl-5">
            <div className="card-body load">
            {/*<h2 className="some-text text-black text-center">Novo cliente</h2>
            <br/>*/}
              {this.state.error && <Message type="danger" text={this.state.error} />}
              <label htmlFor="name" className="grey-text font-weight-light title-card">Nome <i className="fas fa-user"></i></label>
              <input autoComplete="off" autoFocus required type="text" id="name" name="name" className="form-control" ref={input => this._input = input}/>
              <br/>
              <div className="text-center">
                <FlatButton text="Cadastrar " icon="fas fa-user-check" type="submit" />
                <Link to="/stamp">
                  <FlatButton icon="fas fa-arrow-left" text="Voltar " type="button"/>
                </Link>
              </div>
            </div>
          </div>
        </form>
        <br />
      </MainPanel>
    )
  }
}

export default FirstBuy;