import React, { Component, Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import WOW from 'wowjs';
import { checkToken, getEnterpriseId, getOffers, getSession } from '../utils/local-storage';
import RequestHelper from '../utils/request-helper';
import { clientsByInterpriseId, clientsByStampQuantity } from '../utils/urls';
import MainPanel from './components/MainPanel';

class Clients extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      clients: [],
      offer: getOffers()[0]
    }
  }

  componentDidMount() {
    this.WOW.init();
    this.fetchClients();
  }

  fetchClients = async (quantity = null) => {
    let url = "";
    let data = {};
    const enterpriseId = getEnterpriseId();
    if(!quantity) {
      url = clientsByInterpriseId;
      data.enterpriseId = enterpriseId;
    } else {
      url = clientsByStampQuantity;
      data.enterpriseId = enterpriseId;
      data.stampQuantiy = quantity;
    }
    await RequestHelper('post', url, data, 'load', true)
    .then(res => {
      ////console.log(res)
      this.setState(() => ({
        clients: res
      }))
    })
    .catch(err => {
      //console.log(err);
    })
  }

  handleSelectOffer = event => {
    const offer = parseInt(event.target.value, 10);
    this.setState(() => ({offer: getOffers().find(off => off.id === offer)}));
  }

  showMessage = (client) => {
    
    const clientCard = client.finalClientCards.find(card => card.offerId === this.state.offer.id);
    const reward = clientCard.rewards.sort((x, y) => x.rewardRequiredValue - y.rewardRequiredValue).find(rew => !rew.rewardIsReceived);
    
    let maxStamps = reward.rewardRequiredValue;
    let enterpriseName = getSession().enterprise.name;
    let stampsLeft = maxStamps - clientCard.currentStampQuantity;

    let baseURL = `https://wa.me/55${client.finalClientPhone}?text=`;
    let messageComplete = baseURL + encodeURI(`Olá ${client.finalClientName}, voce completou o seu cartão. Que tal vir na *${enterpriseName}* receber seu premio? ;)`)
    let messageNotComplete = baseURL + encodeURI(`Olá ${client.finalClientName}, faltam ${stampsLeft} carimbos para você completar o seu cartão. Que tal vir na *${enterpriseName}* para obter mais carimbos? =)`)
    
    return (
      <div className="col-sm-2 col-md-2 col-xs-12"> 
        <a 
        className="btn btn-xs btn-success btn-wpp" 
        href={ stampsLeft > 0 ? messageNotComplete : messageComplete }
        target="_blank">
          <i className="fab fa-whatsapp fa-2x" aria-hidden="true"/>
        </a>
      </div>);
  }

  checkPromoType = (card) => {    
    switch(card.offerType) {
      case "POINTS":
        return (
          <Fragment key={card.id}>
            <span className="list-star">
              {new Array(card.currentStampQuantity).fill().map ((star, index) => 
                <i key={index} className="fas fa-star star-list-client"></i> 
              )}
            </span>
          </Fragment>
        )
      default:
        return (
          <Fragment key={card.id}>
            <span className="number" >R$ {`${card.currentStampQuantity}`} de {card.maxStampQuantity} </span>
          </Fragment>
        )
    }
  }

  render() {
    if(checkToken()) return <Redirect push to="/"/>

    const { clients } = this.state;

    return(
      <MainPanel>
        <br/>
        <div className="container load">
          <div className="row">
            <div className="col-md-12 p-0">
              <h2 className="h2-responsive text-center text-white"><strong>Clientes</strong></h2>
              <div className="card stamp m-2 p-0">
                <h4 className="text-center card-header">
                  <span>Promoção: </span>
                  <select
                    className="mdb-select large-select__no-width col-sm-9"
                    defaultValue={this.state.offer.id}
                    onChange={this.handleSelectOffer}
                  >
                    {getOffers().map(offer => {
                      return <option value={offer.id} key={offer.id}>{offer.name}</option>
                    })}
                  </select>
                </h4>
                
                <ul className="list-group">
                  { clients.map( client => {
                    const clientCard = client.finalClientCards.find(card => card.offerId === this.state.offer.id);
                    if (!clientCard) return '';
                    return (
                      <li className="list-group-item col-sm-12" key={client.finalClientId}>
                        <div className="row">
                          <div className="col-sm-6 col-md-6 col-xs-12 name-list-client">{client.finalClientName}<span id="star-divider" > | </span>
                            {
                              <div>{this.checkPromoType(clientCard)}</div> 
                            }
                          </div>
                          <br/>
                          <div className="col-sm-4 col-md-4 col-xs-12 number">{client.finalClientPhone}
                          </div>
                          <br/><br/>
                          {this.showMessage(client)}
                        </div>
                      </li>
                    );})
                  }
                </ul>
              </div>
            </div>
          </div>
        </div>
      </MainPanel>
    )
  }
}

export default Clients;