import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import WOW from 'wowjs';
import { checkToken, getOffers } from '../utils/local-storage';
import MainPanel from './components/MainPanel';
import SelectableOfferCard from './components/SelectableOfferCard';

class FirstTime extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.clientData = this.props.location.clientData;
    this.enterpriseOffers = getOffers();
  }

  componentDidMount(){
    this.WOW.init();
  }

  render() {
    if(checkToken()) return <Redirect push to="/"/>
    
    return (
      <MainPanel>
        <div className="load">
          <h4 className="text-center card p-3">Escolha a promoção</h4>
          {this.enterpriseOffers.map(offer => {
            return (
              <SelectableOfferCard offerData={offer} clientData={this.clientData} key={offer.id}/>
            );
          })}
        </div>
      </MainPanel>
    )
  }

}

export default FirstTime;
