import React, {Component} from 'react';

export default class FlatButton extends Component {

  render() {
    return (
      <div className={`text-center ${this.props.extraClass}`} data-wow-delay="0.6s">
        <button className="btn button-accent btn-rounded" onClick={this.props.click} type={this.props.type} disabled={this.props.disabled}>{this.props.text}
          <i className={this.props.icon}></i>
        </button>
    </div>
    )
  }
}
