import React, {Component} from 'react';
import { ResponsiveContainer, BarChart, XAxis, YAxis, Tooltip, Bar, Legend} from 'recharts';

class StackedBarChart extends Component {
  
    render () {
      return (
        <ResponsiveContainer>
          <BarChart width={600} height={300} data={this.props.list} margin={{bottom: 50}}>
            <XAxis dataKey="Dia"/>
            <YAxis label={{value: 'Carimbos', angle: -90, position: 'insideLeft' }}/>
            <Tooltip
                labelFormatter={(value) => 'Dia ' + value}
                formatter={(value, name, props) => {
                props.name = name;
                return ` ${value}`
                }}
                separator=""
            />
            <Legend />
            <Bar dataKey="Antigos" stackId="a" fill="#34c2e0" barSize={5}/>
            <Bar dataKey="Novos" stackId="a" fill="#fdbe46" barSize={5}/>
          </BarChart>
        </ResponsiveContainer>
      );
    }
  }

  export default StackedBarChart;