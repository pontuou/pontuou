import React from 'react';
import { getEnterpriseName } from '../../utils/local-storage';

export default props => {
  if (props.list.length === 0) {
    return (
      <div className="info">Não há dados para serem exibidos</div>
    )
  }
  const clients = props.list.filter(c => !!c.lastVisit)
  return (
    <table className="table table-sm table-striped">
      <thead>
        <tr className="text-dark">
          <th>Cliente</th>
          <th>Último dia pontuado</th>
          <th colSpan="2">Telefone</th>
        </tr>
      </thead>
      <tbody>
        {clients.map(client => {
          const {finalClientUI : phone, finalClientEmail : name} = client;
          const enterpriseName = getEnterpriseName();
          const baseURL = `https://wa.me/55${phone}?text=${name}, que tal vir na ${enterpriseName} aumentar seus pontos? ;)`;
          return (
            <tr key={phone}>
              <td>{name}</td>
              <td>{client.lastVisit.formattedDate}</td>
              <td>{phone}</td>
              <td>
                <a 
                  className="btn btn-sm btn-success btn-wpp" 
                  href={ baseURL }
                  target="_blank">
                  <i className="fab fa-whatsapp fa-2x" aria-hidden="true"/>
                </a>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}