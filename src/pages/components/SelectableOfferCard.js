import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import RequestHelper from '../../utils/request-helper';
import { urlCreateCard, urlExistsClient } from '../../utils/urls';
import { getEnterpriseId } from '../../utils/local-storage';


class SelectableOfferCard extends Component{

  constructor(props){
    super(props)
    
    this.state = {
      hasCard: undefined,
      clientData: this.props.clientData
    }

    this.offerData = this.props.offerData;
  }

  checkCards = id => {
    const finalClientUI = this.state.clientData.finalClientUI;
    const enterpriseId = getEnterpriseId();

    RequestHelper('post', urlExistsClient, {finalClientUI, enterpriseId}, 'load', true)
    .then(res => {
      //console.log(res)
      // Find user card for the selected offer
      const card = res.cards.find(card => card.offerId === id);
      const offerId = this.offerData.id;
      const finalClientId = this.state.clientData.finalClientId;

      if (!card) {
        RequestHelper('post', urlCreateCard, {finalClientId, offerId}, 'load', true)
        .then(resp => {
          //console.log(resp);
          this.setState(() => ({
            clientData: resp,
            hasCard: true
          }))
        })
        .catch(err => {
          //console.log(err);
        })
      } else {
        this.setState(() => ({
          clientData: res,
          hasCard: true
        }));
      }
    })

    ////console.log(card);
  }

  render() {
    const offer = this.offerData;
    //console.log(this.state);
    
    if (this.state.hasCard) 
      return <Redirect push to={{
        pathname: "/confirm-stamp",
        offerData: this.offerData,
        clientData: this.state.clientData
      }}/>
    return (
      <div 
       onClick={event => this.checkCards(offer.id)}
       className="card m-5 hoverable selectable-card">
        <div className="card-body">
          <i className="fas fa-angle-right"></i>
          <span className="ml-1">{offer.name}</span>
          <hr/>
          <span className="small d-block">{offer.description}</span>
        </div>
      </div>
    );
  }
}

export default SelectableOfferCard;