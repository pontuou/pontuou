import React from 'react';

export default function Badge(props) {
  let type = '';
  switch(props.type) {
    case 'info':
      type = "badge badge-info col-sm-12";
      break;
    case 'warning':
      type = "badge badge-warning col-sm-12";
      break;
    case 'success':
      type = "badge badge-success col-sm-12";
      break;
    case 'danger':
      type = "badge badge-danger col-sm-12";
      break;
    case 'primary':
      type = "badge badge-primary col-sm-12";
      break;
    case 'secondary':
      type = "badge badge-secondary col-sm-12";
      break;
    case 'dark':
      type = "badge badge-dark col-sm-12";
      break;
    default:
      type = "none";
  }
  return (
    <p className={type}>
      {props.text}
      {props.children}
    </p>
  );
}
