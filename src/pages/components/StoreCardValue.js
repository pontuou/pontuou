import React, {Component} from 'react';

class StoreCardValue extends Component {
  render () {
    return (
      <div className="align-center col-8 m-auto">
    {/*<progress max={this.props.maximum} value={this.props.real} className="w-100"></progress>*/}
        <div className="w-100 progress-master">
          <div style={{width: ((this.props.real / this.props.maximum) * 100) + '%'}} className="progress-child text-center">
            <span className="d-inline-block" >
              {`${Math.round(((this.props.real / this.props.maximum) * 100))}%`}
            </span>
          </div>
        </div>
        <h6 className="number">R$ {this.props.real} de {this.props.maximum}</h6>
      </div>
    )
  }
}

export default StoreCardValue;