import React, {Component} from 'react';
import {Link, Redirect} from 'react-router-dom';
import { clearAllKeys } from '../../utils/local-storage';

class TopMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    }
  }

  clearToken = event => {
    event.preventDefault();
    clearAllKeys();
    this.setState(() => ({redirect: true}));
  }

  render() {
    if (this.state.redirect) {
      return <Redirect push to="/"/>
    }
    return (
      <nav className="navbar">
        <a className="navbar-brand" disabled href="/"><span className="font-logo" >pontuou</span></a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#my-navbar" aria-controls="my-navbar" aria-expanded="false" aria-label="Toggle navigation"><div className="animated-icon4"><span></span><span></span><span></span></div></button>
        <div className="collapse navbar-collapse" id="my-navbar">
          <ul className="navbar-nav mr-auto" >
            <li className="nav-item item-init">
              <Link to="/">
                <span className="nav-link"> Início <i className="fas fa-home icon-nav"></i><span className="sr-only">(current)</span></span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/clients">
                <span className="nav-link"> Clientes <i className="fa fa-users icon-nav"></i></span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/dashboard">
                <span className="nav-link"> Relátorios <i className="fas fa-chart-bar icon-nav"></i></span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/clients-since">
                <span className="nav-link"> Clientes desde... <i className="fas fa-chart-bar icon-nav"></i></span>
              </Link>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/" onClick={this.clearToken}> Sair <i className="fas fa-sign-out-alt icon-nav "></i></a>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default TopMenu;