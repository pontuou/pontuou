import React from 'react';

export default function Message(props) {
  let type = '';
  switch(props.type) {
    case 'info':
      type = "message bg-info";
      break;
    case 'warning':
      type = "message bg-warning";
      break;
    case 'success':
      type = "message bg-success";
      break;
    case 'danger':
      type = "message bg-danger";
      break;
    default:
      type = "none";
  }
  return (
    <p className={type}>
      {props.text}
      {props.children}
    </p>
  );
}
