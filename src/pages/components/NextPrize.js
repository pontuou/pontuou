import React from 'react';
import Badge from './Badge';

export default props => {
  const orderedList = props.card.rewards.sort((x, y) => x.rewardRequiredValue - y.rewardRequiredValue)
  const nextPrize = orderedList.find(prize => !prize.rewardIsReceived )

  let nextMessage;

  switch(props.offer.offerType) {
    case 'VALUE':
      nextMessage = `Próximo prêmio com R$ ${nextPrize.rewardRequiredValue}.`;
      break;
    default:
      nextMessage = `Próximo prêmio com ${nextPrize.rewardRequiredValue} pontos.`;
  }

  return (
    <Badge text={nextMessage} type="info">
      <span className="d-block">
        {nextPrize.rewardDescription} <i className="fas fa-gift"></i>
      </span>
    </Badge>
  )
}