import React from 'react';
import Check from './Check';

export default (props) => {
  
  let calculateChecks = (real, maximum, selected) => {
    const checks = [];
    let typeStar;
    for(let i = 0; i < maximum; i++) {
      if(i < real) typeStar = 'star';
      else if(i < (real + selected)) typeStar = 'pre-star';
      else if(i >= real) typeStar = 'no-star';
      checks.push(
        <Check 
          type={typeStar} 
          key={i} 
        />
      );
    }
    return checks;
  }

  return (
    <React.Fragment>
      <div className="">
        {calculateChecks(props.real, props.maximum, props.amountSelected)}
      </div>
      <br/>
    </React.Fragment>
  )
}