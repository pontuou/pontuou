import React, {Component} from 'react';
import TopMenu from './TopMenu';
import { checkToken } from '../../utils/local-storage';

export default class MainPanel extends Component{

  render() {
    return (

      <div className=" my-background">
        {!checkToken() && <TopMenu />}
          <div className="container">
            <div className="row horizontalCenter">
              <div className="col-md-12 ">
                
                <div>
                   {this.props.children}
               </div>

              </div>
            </div>
          </div>
      </div>
    )
  }
}
