import React from 'react';

import no_star from '../../images/pre_star2.png';
import pre_star from '../../images/pre_star.png';
import star from '../../images/star.png';

export default function Check (props) {
  let starImg;
  switch (props.type) {
    case 'no-star':
      starImg = no_star;
      break;
    case 'pre-star':
      starImg = pre_star;
      break;
    default:
      starImg = star;
      break;
  }
  return <img src={starImg} className="stars" alt="Carimbo"/>;
}