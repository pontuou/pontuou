import React, {Component} from 'react';
import { ResponsiveContainer, BarChart, YAxis, Tooltip, Bar, Legend} from 'recharts';

class ClientsSinceBarChart extends Component {
  
    render () {
      return (
        <ResponsiveContainer>
          <BarChart width={50} height={300} data={this.props.list} margin={{bottom: 50}}>
            <YAxis dataKey="Quantidade"/>
            <Tooltip
                labelFormatter={(value) => ''}
                formatter={(value, name, props) => {
                props.name = name;
                return ` ${value}`
                }}
                separator=""
            />
            <Legend />
            <Bar dataKey="Quantidade" stackId="a" fill="#34c2e0" barSize={5}/>
          </BarChart>
        </ResponsiveContainer>
      );
    }
  }

  export default ClientsSinceBarChart;