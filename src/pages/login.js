import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import WOW from 'wowjs';
import { setFocus } from '../utils/helpers';
import { clearKey, setSession, setToken, tokenTag, getEnterpriseId, setEnterpriseUrlLogo } from '../utils/local-storage';
import RequestHelper from '../utils/request-helper';
import { urlLogin } from '../utils/urls';
import FlatButton from './components/FlatButton';
import MainPanel from './components/MainPanel';
import Message from './components/Message';


class Login extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      logged_in: undefined,
      error: undefined
    }
    this._email = null
  }

  componentDidMount(){
    this.WOW.init();
    setFocus(this._email)
  }

  handleSubmit = async (event) => {
    event.preventDefault();

    localStorage.clear();

    const username = event.target.elements.email.value;
    const password = event.target.elements.password.value;

    await RequestHelper('post', urlLogin, {username, password}, )
    .then(res => {
      setSession(res);
      setToken(res.token);
      setEnterpriseUrlLogo(res.enterprise.logoUrl);
      this.setState(() => {
        return {
          logged_in: true
        }
      })
    })
    .catch(err => {
      
      clearKey(tokenTag);
      this.setState(() => ({ error: err.data.message }))
      setTimeout(() => {
        this.setState(() => {
          return {
            error: undefined
          }
        })
      }, 5000)
    })

  }

  componentDidUpdate() {
    this.WOW.sync();
  }

  render() {
    if (getEnterpriseId() !== false || this.state.logged_in === true) {
      return <Redirect push to="/stamp" />
    }
    return (
      <MainPanel>
      <div className=" aling-center">
        <img  className="logo wow fadeInUp" src="img/logo.png" alt="logo" />
      </div>
      <br/>
      <br/>
        <h1 className="title wow fadeInUp  text-center font-logo ">
          pontuou
        </h1>
        <br/>
        <form onSubmit={this.handleSubmit}>
          <div className="card mx-xl-5">
            <div className="card-body load">
              {this.state.error && <Message type="danger" text={this.state.error} />}
              <label htmlFor="email" className="grey-text font-weight-light">E-mail</label>
              <input
                autoComplete="off"
                required
                type="email"
                id="email"
                name="email"
                className="form-control"
                ref={email => this._email = email}
              />
              <br/>
              <label htmlFor="password" className="grey-text font-weight-light">Senha</label>
              <input
                required
                type="password"
                id="password"
                name="password"
                className="form-control"
              />
              <div className="text-center py-4 mt-3">
                <FlatButton type="submit" text="Entrar" icon="fas fa-sign-in-alt ml-2" />
              </div>
            </div>
          </div>
        </form>
      </MainPanel>
    )
  }
}

export default Login;
