import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import WOW from 'wowjs';
import { setFocus } from '../utils/helpers';
import { checkToken, getOffers } from '../utils/local-storage';
import RequestHelper from '../utils/request-helper';
import { urlDeliveryPrize, urlStamp } from '../utils/urls';
import FlatButton from './components/FlatButton';
import MainPanel from './components/MainPanel';
import Message from "./components/Message";
import StoreCardPoints from './components/StoreCardPoints';
import StoreCardValue from './components/StoreCardValue';
import NextPrize from './components/NextPrize';

class ConfirmStamp extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      message: undefined,
      stampAdded: false,
      redirect: false,
      starAmount: 0,
      clientData: this.props.location.clientData
    }
    this.offerData = this.props.location.offerData;
    this.messages = null;
    this._starAmount = null;
  }

  componentDidMount(){
    this.WOW.init();
    setFocus(this._starAmount);
  }

  componentWillUnmount(){
    clearInterval(this.messages)
  }

  stamp = event => {
    event.preventDefault();

    if(this.state.starAmount === 0){
      this.showMessage('Adicione pelo menos 1 ponto', 'danger');
      return;
    }

    const finalClientId = this.state.clientData.finalClientId;
    const stampQuantity = event.target.elements.starAmount.value;
    const offerId = this.offerData.id;

    RequestHelper('post', urlStamp, {finalClientId, offerId, stampQuantity}, 'load', true)
    .then(res => {      
      ////console.log(res);
      
      this.setState(() => ({clientData: res, starAmount: 0}));

      if(this.offerData.offerType === "POINTS")
        this.showMessage('Carimbo(s) adicionado(s) com sucesso!', "success");
      else
        this.showMessage('Valor adicionado com sucesso!', "success");

      if(this._starAmount)
        this._starAmount.value = 0;
    })
    .catch(err => {
      this.showMessage(err.data[0].userMessage, "danger");
    });
  }

  deliveryPrize = () => {

    const finalClientId = this.state.clientData.finalClientId;
    const offerId = this.offerData.id;

    RequestHelper('post', urlDeliveryPrize, {finalClientId, offerId}, 'load', true)
    .then(res => {      
      this.setState(() => ({
          clientData: res,
          starAmount: 0
        })
      );
      this.showMessage('Prêmio entregue com sucesso!', "success")
    })
    .catch(err => {
      //console.log(err);      
      this.showMessage(err.data.errors, "danger");
    })
  }

  showMessage(message, msgType) {
    this.setState(() => ({ message, msgType }));
  }

  handleMinusClick = event => {
    event.preventDefault();
    this._starAmount.value > 0 ? this._starAmount.value = this._starAmount.value - 1 : this._starAmount.value = 0;
    this.handleChangeStarAmount();
  }

  handlePlusClick = event => {
    event.preventDefault();
    const card = this.state.clientData.cards.find(card => card.offerId === this.offerData.id);
    const starsLeft = this.offerData.quantity - card.currentStampQuantity;
    this._starAmount.value < starsLeft -1 ? this._starAmount.value = parseInt(this._starAmount.value, 10) + 1 : this._starAmount.value = starsLeft;
    this.handleChangeStarAmount();
  }

  handleChangeStarAmount = event => {
    event && event.persist();
    this.setState(() => ({starAmount: parseInt(this._starAmount.value, 10)}))
  }

  showButtons = () => {    
    const offer = this.offerData;
    const card = this.state.clientData.cards.find(card => card.offerId === offer.id);

    const maxStampQuantity = offer.quantity;
    const currentStampQuantity = card.currentStampQuantity;
    let shouldDelivery = false; 

    if (card.rewards.length === 1)
      shouldDelivery = currentStampQuantity === maxStampQuantity;
    else {
      const rewards = card.rewards.filter(reward => !reward.rewardIsReceived && 
        reward.rewardOfferId === offer.id && 
        currentStampQuantity >= reward.rewardRequiredValue);
      shouldDelivery = rewards.length > 0;
    }

    if(!shouldDelivery){
      if(this.state.stampAdded) return null;
      return (
        <React.Fragment>
          <hr/>
          <h5>Quantidade</h5>
          {offer.offerType === "POINTS" ? this.pointsInput() : this.valueInput()}
          <br/><br/>
          <FlatButton icon="fas fa-plus-circle ml-2" text="Confirmar" type="submit"/>
        </React.Fragment>
      )
    }else{
      return <FlatButton icon="fas fa-gift  ml-2" text="Entregar Prêmio" click={this.deliveryPrize} type="button"/>
    }
  }

  pointsInput = () => {
    return (
      <React.Fragment>
        <button onClick={this.handleMinusClick} className="iconButton">
          <i className="fas fa-minus"></i>
        </button>
        <input 
          autoComplete="off"
          disabled
          type="number" 
          min="0" 
          id="starAmount" 
          name="starAmount" 
          className="input-qtd-start " 
          defaultValue="0" 
          onKeyUp={this.handleChangeStarAmount} 
          onChange={this.handleChangeStarAmount}
          ref={input => this._starAmount = input}
        />
        <button onClick={this.handlePlusClick} className="iconButton">
          <i className="fas fa-plus"></i>
        </button>
      </React.Fragment>
    )
  }

  valueInput = () => {
    return (
      <React.Fragment>
        <input 
          autoComplete="off"
          type="number" 
          min="0" 
          id="starAmount" 
          name="starAmount" 
          className="input-value-start " 
          defaultValue="0" 
          onKeyUp={this.handleChangeStarAmount} 
          onChange={this.handleChangeStarAmount}
          ref={input => this._starAmount = input}
        />  
      </React.Fragment>
    )
  }

  checkPromoType = () => {
    const type = this.offerData.offerType;
    const offer = this.offerData;
    const clientCard = this.state.clientData.cards.find(card => card.offerId === offer.id);
    //console.log(offer);
    //console.log(this.state.clientData);
    
    const currentStampQuantity = clientCard.currentStampQuantity;
    ////console.log(type);
    switch(type){
      case "POINTS":
        return (
          <React.Fragment>
            <div className="div-stars aling-center" >
              <StoreCardPoints 
                real={currentStampQuantity} 
                maximum={offer.quantity} 
                amountSelected={this.state.starAmount}
                />
            </div>
            <div className="text-center">
              {this.showButtons()} 
            </div>
          </React.Fragment>
        )
      default:
        return (
          <React.Fragment>
            <div className="aling-center" >
              <StoreCardValue
                real={currentStampQuantity} 
                maximum={offer.quantity} 
                amountSelected={this.state.starAmount}
                />
            </div>
            <div className="text-center">
              {this.showButtons()} 
            </div>
          </React.Fragment>
        )
    }
  }

  checkOffers = () => {
    if (getOffers().length > 1) {
      
      return (
        <Link to={{
          pathname: "/choose-offer",
          clientData: this.state.clientData
        }}>
          <FlatButton icon="fas fa-arrow-left" text="Trocar promoção " type="button"/>
        </Link>
      )
    }
  }

  render() {
    
    if(checkToken()) return <Redirect push to="/"/>

    if(!this.state.clientData || this.state.redirect) {
      return <Redirect to="/stamp" />
    }
    const data = this.state.clientData;
    const card = this.state.clientData.cards.find(card => card.offerId === this.offerData.id);
    return (
      <React.Fragment>
      <MainPanel>
        <form onSubmit={this.stamp} >
          <div className=" confirm-stamp card mx-xl-5">
            <div className="card-body text-center text-black load">
              {this.state.message && <Message type={this.state.msgType} text={this.state.message} />}
              <NextPrize card={card} offer={this.offerData} />
              <h4>{data.finalClientEmail}</h4>
              <h4 className="number">{data.finalClientUI}</h4>
              {this.checkPromoType()}
              <div className="text-center">
                <Link to="/stamp">
                <FlatButton icon="fas fa-arrow-left" text="Voltar " type="button"/>
                </Link>
                {this.checkOffers()}
              </div>
            </div>
          </div>
        </form> 
      </MainPanel>
      </React.Fragment>
    )
  }
}

export default ConfirmStamp;
