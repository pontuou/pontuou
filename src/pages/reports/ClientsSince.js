import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import WOW from 'wowjs';
import { checkToken } from '../../utils/local-storage';
import RequestHelper from '../../utils/request-helper';
import { urlClientsSince } from '../../utils/urls';
import MainPanel from '../components/MainPanel';
import 'react-day-picker/lib/style.css';
import TableClientsSince from '../components/TableClientsSince';

class ClientsSince extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      selectedDate: new Date().toISOString().slice(0,10),
      clients: [],
    }
  }

  componentDidMount() {
    this.WOW.init();
    this.lastVisitSince({target: {value: this.state.selectedDate}})
  }

  lastVisitSince = e => {
    const value = e.target.value;
    const selectedDate = new Date(value.replace(/-/g, "/"));
    const day = selectedDate.getDate();
    const month = selectedDate.getMonth() + 1;
    const year = selectedDate.getFullYear();

    this.setState(() => ({selectedDate: value}))
    
    
    RequestHelper('post', urlClientsSince, { day, month, year }, 'load', true)
    .then(res => {
      this.setState(() => ({clients: res.filter(client => client.lastVisit) }))
    })
    .catch(err => {
      //console.log(err);
    })
  }

  render() {
    if(checkToken()) return <Redirect push to="/"/>
//console.log(this.state);

    return(
      <MainPanel>
        <div className="stamp card mx-xl-5">
          <div className="load">
            <h4 className="text-center card-header">
            Clientes que não voltaram desde 
              <input type="date" value={this.state.selectedDate} onChange={this.lastVisitSince} style={{marginLeft: "10px"}} />
            </h4>
            <div className="card-body load">
              <span className="font-weight-bold">Total: {this.state.clients.length}</span>

              <div className="table-responsive">
                <TableClientsSince list={this.state.clients} />
              </div>

            </div>
          </div>
        </div>
      </MainPanel>
    )
  }
}

export default ClientsSince;