import IMask from 'imask';
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import WOW from 'wowjs';
import { setFocus } from '../utils/helpers';
import { checkToken, getData, getEnterpriseId, getOffers } from '../utils/local-storage';
import RequestHelper from '../utils/request-helper';
import { urlCreateCard, urlExistsClient } from '../utils/urls';
import FlatButton from './components/FlatButton';
import MainPanel from './components/MainPanel';
import Message from "./components/Message";


class Stamp extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    const urlLogo = (getData().enterpriseUrlLogo !== "null") ? getData().enterpriseUrlLogo : 'img/logo.png';
    this.state = {
      client_dont_exists: undefined,
      client_exists: undefined,
      register_now: this.props.location.register_now,
      phoneOk: false,
      urlLogo: urlLogo,
      cardData: undefined,
      clientData: undefined,
      offerData: undefined,
      chooseOffer: false
    }
    this.inputValue = null;
    this._input = null;
  }

  componentDidMount(){
    this.WOW.init();

    //console.log(getSession())

    if (this.state.register_now) {
      setTimeout(() => {
        this.setState(() => {
          return { register_now: undefined }
        })
      }, 5000)
    }

    const element = document.getElementById("phone");
    const mask = {
      mask: '(00) 0 0000 0000',
    }
    if(element && mask) this.inputValue = new IMask(element, mask);
    
    setFocus(this._input)
  }


  stamp = event => {
    event.preventDefault();

    const finalClientUI = this.inputValue.unmaskedValue;
    const enterpriseId = getEnterpriseId();
    const enterpriseOffers = getOffers();

    RequestHelper('post', urlExistsClient, {finalClientUI, enterpriseId}, 'load', true)
    .then(res => {
      // console.log(res);
      if (enterpriseOffers.length > 1) {
        this.setState(() => ({
          chooseOffer: true,
          clientData: res
        }))
        //console.log(enterpriseOffers);    
      } else if (enterpriseOffers.length === 1) {

        const offer = enterpriseOffers[0];
        const offerId = enterpriseOffers[0].id;
        const clientCards = res.cards;
        const card = clientCards.find(card => card.offerId === offerId);
        const finalClientId = res.finalClientId;
        
        if (!card) {

          RequestHelper('post', urlCreateCard, {finalClientId, offerId}, 'load', true)
          .then(resp => {
            this.setState(() => ({
              clientData: resp,
              offerData: offer
            }))
          })

        }else {          
          this.setState(() => ({clientData: res, offerData: offer}));
        }

        //console.log('ok')
      }
    })
    .catch(err => {
      //console.log(err);
      if (err && err.status === 400){
        this.setState(() => {
          return {
            client_dont_exists: finalClientUI
          }
        })
      }
    });
  }

  checkPhone = (event) => {
    this.setState( () => ({phoneOk: this.inputValue.unmaskedValue.length === 11}));
  }

  render() {
    const {urlLogo} = this.state;

    if(checkToken()) return <Redirect push to="/"/>

    if (this.state.chooseOffer) {
      return <Redirect push to={{
        pathname: "/choose-offer", 
        clientData: this.state.clientData
      }} />
    }

    // if (this.state.client_exists && this.state.client_exists.card === null) {
    //   return <Redirect push to={{pathname: "/first-time", clientData: this.state.client_exists}} />
    // }
    if (this.state.clientData) {      
      return <Redirect push to={{
        pathname: "/confirm-stamp", 
        clientData: this.state.clientData, 
        offerData: this.state.offerData
      }} />
    }
    if (this.state.client_dont_exists) {
      return <Redirect push to={{ 
        pathname: "/first-buy", 
        clientData: this.state.client_dont_exists,
        offerData: this.state.offerData
      }} />
    }

    return (
      <MainPanel>
        <figure>
          <img src={urlLogo || 'https://res.cloudinary.com/fidelicard/image/upload/v1541037480/acai.jpg'} className="mx-auto d-block rounded-circle" alt="logo" height="72" width="72"/>
        </figure>
        <form onSubmit={this.stamp}>
          <div className="stamp card mx-xl-5">
            <div className="card-body load">
              {this.state.register_now && <Message type="success" text="Cliente registrado! 1º estrela adicionada." />}
              <label 
                htmlFor="phone" 
                className="text-black font-weight-light title-card">
                Telefone
              </label>
              
              <input
                autoComplete="off"
                required={true}
                type="tel"
                id="phone"
                placeholder="Ex.: 88 9 1234 5678"
                name="phone"
                className="form-control number"
                onKeyUp={this.checkPhone}
                ref={input => this._input = input}
              />

              <br/>
              <FlatButton text="Buscar" icon="fas fa-search ml-2" type="submit" disabled={!this.state.phoneOk}/>
            </div>
          </div>
        </form>
      </MainPanel>
    )
  }
}

export default Stamp;
